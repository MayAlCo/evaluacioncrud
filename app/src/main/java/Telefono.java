import com.orm.SugarRecord;

public class Telefono extends SugarRecord<Telefono> {
    String atributo;
    String tipo;
    String ejemplo;


    public Telefono() {

    }
    public Telefono(String atributo, String tipo, String ejemplo){
        this.atributo = atributo;
        this.tipo = tipo;
        this.ejemplo = ejemplo;
    }

    public String getAtributo() {
        return atributo;
    }

    public void setAtributo(String atributo) {
        this.atributo= atributo;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo= tipo;
    }

    public String getEjemplo() {
        return ejemplo;
    }

    public void setEjemplo(String atributo) {
        this.ejemplo= ejemplo;
    }
}
