package alvaradocobeamaykelapp.pm.facci.pruebasqlite;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class Lista extends AppCompatActivity {

    ListView listaTelefonos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_lista );

        listaTelefonos = (ListView)findViewById( R.id.listaTelefono );
        cargarListView();

    }

    private void cargarListView(){
        List<Telefono> resultadoConsulta = Telefono.listAll( Telefono.class );
        ArrayList<String> lista = new ArrayList<String>(  );
        for (int i=0; i<resultadoConsulta.size(); i++){
            Telefono resultado = resultadoConsulta.get( i );
            lista.add( resultado.getImei()+ " - " +resultado.getGama()+ " - " +resultado.getMarca()+ " - " +resultado.getModelo()+ " - " +resultado.getCompañiaTelefonica());
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<>( this, android.R.layout.simple_list_item_1, lista );
        listaTelefonos.setAdapter( adapter );
    }
}
