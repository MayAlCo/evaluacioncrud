package alvaradocobeamaykelapp.pm.facci.pruebasqlite;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    EditText imei, gama, marca, modelo, compañiaTelefonica;
    Button guardar, consultaIndividual, modificar, eliminar, consultaGeneral;
    TextView lbImei, lbGama, lbMarca, lbModelo, lbCompañiaTelefonica;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_main );

        imei = (EditText) findViewById( R.id.imei );
        gama = (EditText) findViewById( R.id.gama );
        marca = (EditText) findViewById( R.id.marca );
        modelo = (EditText) findViewById( R.id.modelo );
        compañiaTelefonica = (EditText) findViewById( R.id.compañiaTelefonica );
        guardar = (Button) findViewById( R.id.btnGuardar );
        consultaIndividual = (Button) findViewById( R.id.btnConsultaIndividual );
        modificar = (Button) findViewById( R.id.btnModificar );
        eliminar = (Button) findViewById( R.id.btnEliminar );
        consultaGeneral = (Button) findViewById( R.id.btnConsultaGeneral );
        lbImei = (TextView) findViewById( R.id.lbImei );
        lbGama = (TextView) findViewById( R.id.lbGama );
        lbMarca = (TextView) findViewById( R.id.lbMarca);
        lbModelo = (TextView) findViewById( R.id.lbModelo);
        lbCompañiaTelefonica = (TextView) findViewById( R.id.lbCompañiaTelefonica);


        guardar.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Telefono telefono = new Telefono( imei.getText().toString(), gama.getText().toString(), marca.getText().toString(), modelo.getText().toString(), compañiaTelefonica.getText().toString());
                telefono.save();


                Toast.makeText( MainActivity.this, "Registro guardado", Toast.LENGTH_SHORT ).show();
            }
        } );

        consultaIndividual.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                List<Telefono> resultado = Telefono.find( Telefono.class, "imei=?", imei.getText().toString() );
                Telefono datos = resultado.get( 0 );
                lbImei.setText( "IMEI" + " = " +  datos.getImei());
                lbGama.setText( "GAMA" + " = " + datos.getGama() );
                lbMarca.setText( "MARCA" + " = " + datos.getMarca() );
                lbModelo.setText( "MODELO" + " = " + datos.getModelo() );
                lbCompañiaTelefonica.setText( "COMPAÑIA TELEFONICA" + " = " + datos.getCompañiaTelefonica() );

            }
        } );



        eliminar.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                List<Telefono> telefonos = Telefono.listAll(Telefono.class);
                Telefono.deleteAll(Telefono.class);

                Toast.makeText( MainActivity.this, "Registro Eliminado", Toast.LENGTH_SHORT ).show();
            }
        } );

        consultaGeneral.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent( MainActivity.this, Lista.class );
                startActivity( intent );
            }
        } );

        modificar.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                 Telefono modificar = Telefono.findById(Telefono.class, (long) Integer.parseInt( imei.getText().toString()) );
                 modificar.imei = imei.getText().toString();
                 modificar.save();



                //Mensaje
                Toast.makeText(MainActivity.this, "Registro actualizado exitosamente", Toast.LENGTH_SHORT).show();

            }
        } );

    }


}
