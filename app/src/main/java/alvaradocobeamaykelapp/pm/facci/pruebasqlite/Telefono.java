package alvaradocobeamaykelapp.pm.facci.pruebasqlite;

import com.orm.SugarRecord;

public class Telefono extends SugarRecord<Telefono> {
    String imei;
    String gama;
    String marca;
    String modelo;
    String compañiaTelefonica;


    public Telefono(){

    }
    public Telefono(String imei, String gama, String marca, String modelo, String compañiaTelefonica){
        this.imei = imei;
        this.gama = gama;
        this.marca = marca;
        this.modelo = modelo;
        this.compañiaTelefonica = compañiaTelefonica;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei= imei;
    }

    public String getGama() {
        return gama;
    }

    public void setGama(String marca) {
        this.marca= marca;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca= marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo= modelo;
    }
    public String getCompañiaTelefonica() {
        return compañiaTelefonica;
    }

    public void setCompañiaTelefonica(String compañiaTelefonica) {
        this.compañiaTelefonica= compañiaTelefonica;
    }
}

